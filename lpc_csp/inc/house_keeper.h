/*
 * house_keeper.h
 *
 *  Created on: Dec 30, 2014
 *      Author: albertasat
 */

#ifndef HOUSE_KEEPER_H_
#define HOUSE_KEEPER_H_
#include <inttypes.h>
#include <stdbool.h>

/* NanoPower housekeeping struct */
typedef struct {
    uint16_t vboost[3]; //! Voltage of boost converters [mV] [PV1, PV2, PV3]
    uint16_t vbatt; //! Voltage of battery [mV]
    uint16_t curin[3]; //! Current in [mA]
    uint16_t cursun; //! Current from boost converters [mA]
    uint16_t cursys; //! Current out of battery [mA]
    uint16_t reserved1; //! Reserved for future use
    uint16_t curout[6]; //! Current out (switchable outputs) [mA]
    uint8_t output[8]; //! Status of outputs**
    uint16_t output_on_delta[8]; //! Time till power on** [s]
    uint16_t output_off_delta[8]; //! Time till power off** [s]
    uint16_t latchup[6]; //! Number of latch-ups
    uint32_t wdt_i2c_time_left; //! Time left on I2C wdt [s]
    uint32_t wdt_gnd_time_left; //! Time left on I2C wdt [s]
    uint8_t wdt_csp_pings_left[2]; //! Pings left on CSP wdt
    uint32_t counter_wdt_i2c; //! Number of WDT I2C reboots
    uint32_t counter_wdt_gnd; //! Number of WDT GND reboots
    uint32_t counter_wdt_csp[2]; //! Number of WDT CSP reboots
    uint32_t counter_boot; //! Number of EPS reboots
    int16_t temp[6]; //! Temperatures [degC] [0 = TEMP1, TEMP2, TEMP3, TEMP4, BP4a, BP4b]*
    uint8_t bootcause; //! Cause of last EPS reset
    uint8_t battmode; //! Mode for battery [0 = initial, 1 = undervoltage, 2 = nominal, 3 = batteryfull]
    uint8_t pptmode; //! Mode of PPT tracker [1=MPPT, 2=FIXED]
    uint16_t reserved2;
} eps_hk_t;

void HouseKeeper_setTestValues(eps_hk_t* test_data);
bool HouseKeeper_equals(eps_hk_t* hk1, eps_hk_t* hk2);

#endif /* HOUSE_KEEPER_H_ */
