# README #

### What is this repository for? ###

This repo is for trying out the Cubesat protocol (csp), specifically communicating between a linux pc and the lpc 1769 board.  The intention is once we figure out how to communicate using csp, the code will be brought into the main albertasat repo.

### How do I get set up? ###

#### Hardware ####

* Linux-based PC or a PC/Mac with a Linux VM.  Two available USB ports are needed.
* LPC1769 board with a USB connector to the board
* A device to adapt the second USB connection to a UART connection, such as a FTDI FT232RL USB to serial IC.  There may be a way to use the built-in FTDI IC on some Arduinos, such as the Uno.
* A USB cable to connect the above device to your computer. 

#### Hardware Setup ####
* Connect the LPC1769 board via USB to your PC.  This powers the board, enabled programming and debugging on the chip.
* Connect the second USB to the USB-to-serial adapter.  
* Connect the USB-to-serial adapter to the LPC's UART3:
    * Connect LPC's pin 9 (UART3 TX) to RX on the USB-to-serial adapter.
    * Connect LPC's pin 10 (UART3 RX) to TX on the USB-to-serial adapter.

Note: UART3 is supposed to be 5v tolerant, but you might want to double check that.

#### Software ####

* Linux OS
* LPCExpresso IDE - this can be licensed freely (with some limits on the free version).

### Build Notes for libcsp ###
* libcsp comes from an open source repository that uses waf as a build system (instead of something like make or Cmake).
* waf is a python program that runs wscript (also python) to configure, build etc.
* wscript has to be modified as you change the build.  For example, I've added the option --with-board=lpc1769 (only option available) to set up the appropriate #defines for the build for the lpc1769 board.
* I've added scripts "wafcommand_linux" and "wafcommand_lpc" to configure the builds for linux (output into Debug_linux) and the lpc1769 (output into Debug_lpc1769)
* You can use these to build and clean e.g. `./wafcommand_linux build` will configure and build for linux
* The Eclipse project has been set up to build / clean libscp using these waf commands.

### Build notes for other projects ###
* The other projects in this repo do not use waf; they use Eclipse's build.
* Note that libcsp's config creates include/csp/csp_autoconfig.h in the libcsp output directory, which just has #defines to configure source code for the build.  This is why the lpc_csp project has libcsp/Debug_lpc1768/include in its include path list, so that these #defines are found and used.
* libcsp does not have these sorts of settings since it does not use Eclipse's regular build system.  Presumably waf handles including the path to the csp_autoconfig.h when it builds.

### Trying it out #1  - echo characters: from linux to lpc1769, received back at linux.  ####
* In this case, CSP is only running on linux; the lpc1769 only echos each character received, so it does no CSP processing at all
* Build Debug configuration of the periph_uart_rb project and run the binary on the LPC1769
* Build Debug_linux configuration of the libcsp project and run the created "kiss" executable.
* You should see several messages in the console.  
    * A ping response time of -1 ms indicates and error, so something is wrong.  Perhaps the uart connections are incorrect, or you need to reset the UART connection from the PC (try unplugging the USB cable and plugging it back in)

### Trying it out #2 - CSP on Linux (server) and LPC1769 (client) ###
* In the libcsp project build the Debug_linux configuration to create the libcsp/Debug_linux/kiss_server executable
* Run kiss_server on the Linux box
* In the libcsp project, build Debug_lpc1769 to create the libcsp for the lpc1769
* Build the lpc_csp project (Debug config) and run on the lpc
* You should see:
    *  The pings working with response times from 50 - 250 ms
    *  Results of "service calls" csp_ps, csp_memfree, csp_buf_free and csp_uptime.  These are made from the lpc1769, and the linux side reponds with its values.
    * "Test response from server: OK" message.  
    * This will loop over and over.

### Who do I talk to? ###

* Repo owner or admin: neil.lamoureux@gmail.com